#!/usr/bin/env bash

docker run --rm -p 4567:4567 -v $(pwd):/wiki gollumwiki/gollum:master --config bin/gollum.config.rb --ref main --mathjax