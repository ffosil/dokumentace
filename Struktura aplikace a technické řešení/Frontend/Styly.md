## Principy stylování 
Při vývoji systému je kombinováno několik způsobů stylování. Při kódování je použit preprocessor SASS. Jedná se o následující přístupy.

### Globální styly
Jedná se o styly, které jsou použity globálně v rámci celého webu. Tyto styly vychází stylopisu ČT definovaných grfickým a UX oddělením. Jsou zde jak barvy, tak typografie (písmo, odstavce, nadpisy)m formulářové prvky apod. 
Jsou uloženy ve složce **styles**

### Komponentové styly
Většina REACT komponent obsahuje stylovací scss soubor, který je napojen na konkrétní komponentu přes lokální import (import styles from). Komponenty primárně využívají globální styly. Pomocí komponentových stylu dochází k úpravám konkrétní komponenty, na které globální styyl nestačí a zároveň není předpoklad že se budou používat jinde. 

### Atomické styly
V souboru frontend/styles/_utility-global.scss je připravena řada atomických tříd, které slouží především k nastavení okrajů (margin) a odsazení (padding). Tyto styly se používají ve chvíli, kdy grafik z nějakého důvodu změní jeden, do té doby globálně používaný styl, ale pouze na jednom místě. Místo vytváření nové komponenty nebo globálního stylu je s výhodou použita atomická třída. 
