Frontend je vytvořen ve frameworku [Next.js](https://nextjs.org) tzn. za použití technologie [React](https://reactjs.org).

Je uložen v monorepo ve složce __frontend__. Je pooužita standardní struktura souborů (components, pages, ...)

Definice závislostí je uložena v souboru __package.json__ a pro jejich správu se používá nástroj [Yarn](https://yarnpkg.com).

## [Styly](Frontend/Styly)
Pro kódování stylů je použit preprocesor [SASS](https://sass-lang.com)