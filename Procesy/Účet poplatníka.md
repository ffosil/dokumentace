Zobrazení účtu poplatníka reaguje na počet připojených VS. Může nastat jedna ze čtyř (resp. pěti) možností. Pokud je na uživatelský účet vázáno jenom jedno VS pak se zobrazuje účet poplatníka jako jednostránkový výpis FO resp. PO. 

Pokud je na uživatelský email vázáno více VS (míst poplatků), pak se mu nejdříve objeví výpis  účtu (email a možnost změny hesla) a je následován výpisem dostupných VS a jejich zkráceným výpisem názvu poplatného místa a jeho aktuálního stavu. Proklikem se lez dostat do detailu konkrétního VS, ve kterém pak již vše funguje jako u výpisu v případě jednoho VS.

V detailu VS v účtu poplatníka si může uživatel upravovat některá nastavení (ale ne všechny). Záleží jednak na časovém období, kdy uživatel může změnit typ platby. Tj. Platbu lze měnit pouze poslední měsíc v aktuálně zvoleném platebním období (viz sekce Interval a počátek platby poplatků).

Tak navíc záleží na to v jakém stavu je aktuálně konkrétní VS. Mohou nastat čtyři stavy:

1. **Oslovený z WPČT**
tj. Uživatel, který si poprvé vytvořil nějakým způsobem účet na webu poplatků ČT, ale ještě neprovedl první nebo autorizační platbu (resp. Dokud není platba zaúčtována v ZIS). Reálně se uživatel stává poplatníkem až v momentě zaúčtování první platby. V úču poplatníka pak může pouze zaplatit první platbu a měnit heslo. 
2. **Aktivní poplatník**
poplatník, který platí a má vše v pořádku zaplaceno, popřípadě, nemá zaplaceno, ale ještě nebyl postoupen do právního řešení. Zde může porvést uživatel všechn úpravy. To znamená:

> * Změna hesla
* Změna typu platby (pokud je ve správném časovém období)
* Přidat nebo upravit číslo účtu
* Vypnout nebo zapnout si zasílat informace o platbě (tzv.pInfo)
* Spravovat kontaktní údaje
* Zaslat autorizovaný požadavek
* U firmy lez zvýšit počet TV. Pro snížení je potřeba použít autorizovaný požadavek

3. **Účet v právním řešení**
dlužnou částku má nyní ve správě účastník v právním řešení (exekutorská nebo advokátní kancelář). V tomto stavu je uživatel pouze informován, že je jeho VS v právním řešení. Nemůže nic měnit. 

4. **Zrušený účet**
v tomto případě nemůže poplatník s účtem nakládat, ale může do něj po dobu 2 měsíce nahlížet.

Kromě úprav si může uživatel v uživatelském účtu stáhnout přehledové PDF (generované v ZIS) popřípadě může porvést autorizovaný požadavek. Jedná se o jednoduchý formuláře, kde uživatel může požádat o radu nebo o změnu, kterou nelze provést přímo v účtu poplatníka. Autorizovaný požadavek je zaslán do OPČT a v kopii i uživateli. OPČT následně požadavek řeší telefonicky a v desktopové aplikaci poplatků.

Z účtu poplatníka má uživatel možnost provést stejnou řadu scénářů jako u neregistrovaného uživatele. Postup scénářů je prakticky stejný, jen není vyžadována autorizace emailu a nastavení hesla. Případně vzniklé nové VS jsou automaticky vázána na přihlášeného uživatele. Rovněž pokud se nepřihlášený uživatel snaží vyřešit nějaký scénář a navázat ho na email, který je již navázán na nějaké VS, je požádán a přesměrován na přihlášení do účtu poplatníka. 