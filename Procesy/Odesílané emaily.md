V rámci systému je nutné rozesílat uživatelům řadu emailových zpráv. Pro vlastní rozesílku se používá ZIS. Nicméně vlastní template se vytváří na backendu WPČT a následně je výsledná HTML posíláno do ZIS. Template jsou uloženy v backend/app/emails/templates/emails. Používá se jednotný base.html kterým jsou obaleny všechny ostatní template. Template se dělí do třech skupin. Obecné emaily označené M, emaily pro opakované platby jednorázové označené MO a emaily pro servisní rozesílky o opakovaných platbách pro OPČT. Seznam emailů je rovněž uveden v přiložené sitemap. 

## Obecné emaily
- **M1_email_authorization_request.html** - autorizace emailu mimo výzvy
- **M1_email_summons_authorization_request.html** - autorizace emailu pouze pro výzvy
- **M2_forgottenpassword_request.html** - zaslání linku pro změnu
- **M3_forgottenpassword_thx.html** - informace a potvrzení změny hesla
- **M4_new_account_approved.html**  - Potvrzení vytvoření nového účtu
- **M6_transfer_from_sipo_to_direct_payment.html** Potvrzení převodu ze SIPO na přímou platbu - s novým VS
- **M7_change_payment_approved.html** potvrzení změny typu platby
- **M8_new_account_approved_household.html** potvrzení registrace - pro domácnost (household)
- **M8_new_account_approved_company.html**  potvrzení registrace - firmy (company)
- **M9_changes_approved_short.html** Změny údajů - bez změny typu platby
- **M9_changes_approved.html** - vč. typu platby
- **M10_authorized_request.html**  Autorizační požadavek
- **M10_authorized_request_for_ct.html**  Autorizační požadavek - specifická kopie pro ČT
- **M11a_summons_answer_no_tv_household.html** Výzva - potvrzení o nevlastnictví TV - domácnost
- **M11a_summons_answer_no_tv_company.html** Výzva - potvrzení o nevlastnictví TV - firma
- **M11b_summons_answer_duplicity.html** Výzva - potvrzení o duplicitě - domácnost
- **M11c_summons_answer_health_reason.html** Výzva - potvrzení o zdravotních důvodech - domácnost
- **M11d_summons_answer_social_reason.html**  Výzva - potvrzení o sociálních důvodech - domácnost
- **M11e_summons_answer_other.html** Výzva - potvrzení o jiném důvodu - domácnost
- **M11h_new_account_approved_company.html**  Výzva - potvrzení nového účtu - firma
- **M13_connect_new_vs.html**  Potvrzení napojení nového VS k účtu
- **M14_registration_of_existing_VS.html** Potvrzení napojení jiného (funkčního) VS k účtu
- **MC1_contact_form_for_user.html** Email jako potvrzení požadavku z kontaktního formuláře
- **MC1_contact_form_for_ct.html**  Email jako potvrzení požadavku z kontaktního formuláře - kopie pro ČT

## Triggery mailů spojené s opakovanými platbami
- **MO2_payment_not_came_1st_try.html** Platba se nám nepodařilo stáhnout - používá se pro všechny pokusy
- **MO3_payment_not_came_2nd_try.html** Druhý pokus o platba se nám nepodařilo stáhnout - nepoužívá se viz MO2
- **MO5_payment_not_came_last_in_month.html** Poslední pokus o platba se nám nepodařilo stáhnout - nepoužívá se viz MO2
- **MO7_card_expired_attention.html** Blíží se expirace vaši karty - změňte ji. 
- **MO8_payment_not_came_card_expired.html** Platba neproběla kvůli expiraci karty
- **MO9_authorization_payment_has_not_been_finished.html**  Autoroizační platba nebyla dokončena + Nepoužívá se!!!
- **SERVIS__trigger_first_monthly_collection_report.html** report pro ČTP o invokaci první opakované platby 14. v měsíc
- **ERVIS__trigger_second_monthly_collection_report.html**  report pro ČTP o invokaci druhé opakované platby 20. v měsíc
