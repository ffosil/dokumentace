## Přihlášení
Pomocí přihlášení, tj. po zadání proprietárního emailu a hesla je uživatel přesunut do účtu uživatele při použití principy přihlášení a účtu poplatníka.

## Registrace
Scénář pro registraci slouží k vytvoření webové účtu, k již stávajícímu variabilnímu symbolu (popř. SIPO). Postup se liší dle typu objektu - fyzická vs. právnická osoba:

### Domácnost
1. Zadání - emailu a potvrzení souhlasu
2. Pomocí odkazu v emailu přejít na autorizační stránku
3. Zadání hesla
4. Ověření poplatníka - zadává se spojovací číslo (VS, nebo SIPO), datum narození, první dvě písmena příjmení

### Firma
1. Zadání - emailu a potvrzení souhlasu
2. Pomocí odkazu v emailu přejít na autorizační stránku
3. Zadání hesla
4. Ověření poplatníka - zadává se spojovací číslo (VS) a IČ

## Přímá platba TV poplatku
Kdokoliv může na stránkách WPČT provést platbu neuhrazeného poplatku. Uživatel zadává částku, VS, specifický symbol (nepovinně), email na který bude zasláno potvrzení a typ online platby (karta, platební brána, google pay)

**Zadání údajů přímé platby**, zde zadá výše požadované údaje. Pokud jsou správné je vygenerována platba a uživatel je přesměrován na platební bránu.

**Stav platby** - uživatel je po zaplacení přesměrován na tuto stránku, kde je seznámen s průběhem platby.

## Zapomenuté heslo
Řešení zapomenutého hesla má čtyři kroky. V prvním kroku je uživatel vyzván k zadání emailu a variabilního symbolu (popř. spojovacího čísla u SIPO). V případě více VS stačí pouze jedno. Pokud uživatel zadá správné údaje, je mu zobrazena stránka s informací o odeslání emailu. V emailu následně obdrží link na nastavení nového hesla. Po kliknutí na link se uživateli otevře stránka s nastavením nového hesla. Po nastavení nového hesla obdrží uživatel potvrzení emailem.