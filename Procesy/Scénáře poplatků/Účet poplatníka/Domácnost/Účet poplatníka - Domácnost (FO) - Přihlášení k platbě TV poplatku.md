Jedná se o základní scénář, kdy se uživatel (FO) dobrovolně rozhodně pro přihlášení se k televizním poplatkům, ale již má webový účet pro jiné VS: 

Zadání osobních údaje: Uživatel zadává své jméno, datum narození (omezeno na min. 18 let), adresu trvalého bydliště jako povinné údaje a tituly kontaktní adresu jako dobrovolné údaje. Adresní údaje jsou přes API kontrolovány v systému RUIAN, který udržuje aktuální platný seznam všech adres v ČR.

**Počátek platby:** uživatel zadává od jakého data se hodlá k platbě přihlásit Je možné zadat datum aktuálního dne nebo jakéhokoliv dne zpět až do doby vzdálené tři roky. 

**Periodicita:** Uživatel si vybere interval platby  - měsíční, čtvrtletní, půlroční nebo roční. Doporučen je měsíční.

**Platební metoda:** Uživatel volí platební metodu - opakovaná platba kartou  (pouze u měsíční periodicity), jednorázově (karta, google pay, platební bránou) a nebo převodem z bankovního účtu (zde je možné nepovinně zadat číslo účtu). 

**Shrnutí:** Uživatel je seznámen se všemi nastaveními které proběhl a je vyzván k finálnímu potvrzení. Popřípadě může volit kroky zpět a nastavení upravit. 

**Dokončení:** Uživatel je informován o stavu zápisu do ZIS a zároveň je mu poslán příslušný email. Dle volby platby je mu nabídnuta možnost zaplatit, autorizovat platbu nebo se vrátit do účtu poplatníka. 

**Stav platby:** Uživatel je po zaplacení přesměrován na tuto stránku, kde je seznámen s průběhem platby.
