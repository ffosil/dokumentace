Jedná se o scénář, kdy se uživatel (FO) na základě výzvy (zaslané dopisem) reaguje. Reagovat může jednak jednou z nabízených odpovědí (resp. Prohlášením), popřípadě založením nového VS a přidáním ke stávajícímu účtu. 

**Ověření:** Na základě výzvy uživatel nastaví VS oslovení, první dvě písmena příjmení a souhlas s podmínkami služby TV poplatky a se zpracováním osobních údajů.  Po ověření je uživatele přesměrován na výběr odpovědí. 

**Nastavení evidence:** Jedná se o rozcestník, kde uživatel určuje, jak chce na výzvu reagovat.

Možnosti jsou:

- **Poplatek je v domácnosti již hrazený a jedná se o duplicitu:** jedná se o chybu sytsému nebo uživatel již poplatek hradí. V tomto kroku potvrdí čestné prohlášení a zapíše platné VS ke kterému je poplatek hrazen,
- **Nevlastním žádný TV přijímač:** čestné prohlášení, že nevlastní TV přijímač. 
- **Splňuji podmínky pro osvobození ze zdravotních důvodů:** čestné prohlášení, že splňuji podmínky pro osvobození ze zdravotních důvodů.
- **Splňuji podmínky pro osvobození ze sociálních důvodů:** čestné prohlášení, že splňuji podmínky pro osvobození ze sociálních důvodů.
- **Dopis byl doručen na chybnou adresu, neznámého adresáta, či zemřelého:** čestné prohlášení, že dopis byl doručen na chybnou adresu, neznámého adresáta, či zemřelého.

**Poděkování:** pokud uživatel vybere jednu z výše uvedených možností a potvrdí ji, Je mu poděkováno. 

Poslední možností je přihlášení se jako nový poplatník, kde se postupuje stejně jako u scénáře Domácnost (FO) - Přihlášení k platbě TV poplatku s tím rozdílem, že již nedochází a k identifikaci osoby (to je součástí výzvy a načítá se ze ZISu).

Kroky jsou:

- **Počátek platby:** uživatel zadává od jakého data se hodlá k platbě přihlásit Je možné zadat datum aktuálního dne nebo jakéhokoliv dne zpět až do doby vzdálené tři roky. 
- **Periodicit:** Uživatel si vybere interval platby  - měsíční, čtvrtletní, půlroční nebo roční. Doporučen je měsíční.
- **Platební metoda:** Uživatel volí platební metodu - opakovaná platba kartou  (pouze u měsíční periodicity), jednorázově (karta, google pay, platební bránou) a nebo převodem z bankovního účtu (zde je možné nepovinně zadat číslo účtu). 
- **Shrnutí:** Uživatel je seznámen se všemi nastaveními které proběhl a je vyzván k finálnímu potvrzení. Popřípadě může volit kroky zpět a nastavení upravit. 
- **Dokončení:** Uživatel je informován o stavu zápisu do ZIS a zároveň je mu poslán příslušný email. Dle volby platby je mu nabídnuta možnost zaplatit, autorizovat platbu nebo se vrátit do účtu poplatníka.
- **Stav platby:** Uživatel je po zaplacení přesměrován na tuto stránku, kde je seznámen s průběhem platby.