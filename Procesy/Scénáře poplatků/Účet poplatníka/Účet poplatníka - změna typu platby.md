Pokud je účet poplatníka ve stavu, kde může měnit platbu (tj, poplatník je aktivní, není v právním řešení a jeho nastavená periodicita platby je v posledním měsíci před novou platbou). Změna platby není závislá na typu subjektu (FO / PO). Skládá se z následujících kroků.

**Periodicita:** Uživatel si vybere interval platby  - měsíční (pouze FO), čtvrtletní, půlroční nebo roční. 

**Platební metoda:** Uživatel volí platební metodu - opakovaná platba kartou  (pouze u měsíční periodicity a FO), jednorázově (karta, google pay, platební bránou) a nebo převodem z bankovního účtu (zde je možné nepovinně zadat číslo účtu). 

**Shrnutí:** Uživatel je seznámen se všemi nastaveními, které proběhl a je vyzván k finálnímu potvrzení. Popřípadě může volit kroky zpět a nastavení upravit. 

**Dokončení:** Uživatel je informován o stavu zápisu do ZIS a zároveň je mu poslán příslušný email. Dle volby platby je mu nabídnuta možnost zaplatit, autorizovat platbu nebo odejít do účtu poplatníka. 

**Stav platby:** Uživatel je po zaplacení přesměrován na tuto stránku, kde je seznámen s průběhem platby.