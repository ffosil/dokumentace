Pokud je u VS evidován nedoplatek a zároveň VS není v právním řešení, může uživatel uhradit platbu online (bez ohledu na typ platby). Je mu nabídnut buď QR kód platby nebo výběr online platební metody a následně přesměrování na platební bránu. 

**Výběr platební metody a QR kód:**  Na této stránce si uživatel volí QR kód platby nebo výběr online platební metody a následně je přesměrován na platební bránu (v případě volby online platby).

**Stav platby:** uživatel je po zaplacení přesměrován na tuto stránku, kde je seznámen s průběhem platby.