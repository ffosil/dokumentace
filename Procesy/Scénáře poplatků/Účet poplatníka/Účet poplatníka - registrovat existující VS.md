Pokud má již uživatel exitující VS a chce ho připojit ke stávajícímu účtu, lze to provést v následujícím scénáři. 

**Propojení evidence s účtem poplatníka:** pro FO zadává uživatel spojovací číslo SIPO nebo variabilní symbol, datum narození, ověření poplatníka (první dvě písmena příjmení a souhlasím s podmínkami a se zpracováním osobních údajů. Naproti tomu PO zadává VS a IČ. Po ověření správnosti údajů (exitující údaje a VS není vázán na jiného uživatele) je uživatel přesměrován na dokončovací stránku.

**Dokončení:** uživatel je informován, že propojení proběhlo a jaké je nastavení přidaného VS,