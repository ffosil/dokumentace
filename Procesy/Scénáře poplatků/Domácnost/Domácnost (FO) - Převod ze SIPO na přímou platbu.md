Tento scénář slouží uživatelům, kteří se rozhodnou nadále nevyužívat pro platbu systém SIPO, chtějí platit jiným způsobem a zároveň ještě nemají účet. Pokud chce uživatel pouze vytvořit k SIPO webový účet, může tak učinit pomocí klasické registrace. Kromě úvodní stránky se propojení skládá ze sedmy kroků.

**Ověření SIPO:** uživatel zadává spojovací číslo SIPO, datum narození, první dvě písmena příjmení, kontaktní email a souhlas se zpracováním os. údajů. V případě že jsou údaje správné je uživateli zaslán email s autorizačním linkem. 

**Natavení hesla:** uživatel si pro účet nastaví heslo. 

**Periodicita:** Uživatel si vybere interval platby  - měsíční, čtvrtletní, půlroční nebo roční. Doporučen je měsíční.

**Platební metoda:** Uživatel volí platební metodu - opakovaná platba kartou  (pouze u měsíční periodicity), jednorázově (karta, google pay, platební bránou) a nebo převodem z bankovního účtu (zde je možné nepovinně zadat číslo účtu). 

**Shrnutí:** Uživatel je seznámen se všemi nastaveními které proběhl a je vyzván k finálnímu potvrzení. Popřípadě může volit kroky zpět a nastavení upravit. 

**Dokončení:** Uživatel je informován o stavu zápisu do ZIS a zároveň je mu poslán příslušný email. Dle volby platby je mu nabídnuta možnost zaplatit, autorizovat platbu nebo odejít do účtu poplatníka. 

**Stav platby:** Uživatel je po zaplacení přesměrován na tuto stránku, kde je seznámen s průběhem platby.
