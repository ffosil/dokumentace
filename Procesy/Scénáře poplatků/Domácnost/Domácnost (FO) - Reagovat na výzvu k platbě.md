Jedná se o scénář, kdy se uživatel (FO) na základě výzvy (zaslané dopisem) reaguje. Reagovat může jednak jednou z nabízených odpovědí (resp. Prohlášením), popřípadě založením nového účtu poplatníka.  Kromě úvodní informační stránky a stránek pro registraci nového poplatníka tento scénář obsahuje 8 kroků. 

**Ověření:** Na základě výzvy uživatel nastaví VS oslovení, kontaktní email a první dvě písmena příjmení.  Po ověření je uživatele zaslán autorizační link  pro pokračování.

**Nastavení evidence:** Jedná se o rozcestník, kde uživatel určuje jak chce na výzvu reagovat. Možnosti jsou:

* **Poplatek je v domácnosti již hrazený a jedná se o duplicitu:** jedná se o chybu sytsému nebo uživatel již poplatek hradí. V tomto kroku potvrdí čestné prohlášení a zapíše platné VS ke kterému je poplatek hrazen,
* **Nevlastním žádný TV přijímač:** čestné prohlášení, že nevlastní TV přijímač. 
* **Splňuji podmínky pro osvobození ze zdravotních důvodů:** čestné prohlášení, že splňuji podmínky pro osvobození ze zdravotních důvodů.
* **Splňuji podmínky pro osvobození ze sociálních důvodů:** čestné prohlášení, že splňuji podmínky pro osvobození ze sociálních důvodů.
* **Dopis byl doručen na chybnou adresu, neznámého adresáta, či zemřelého:** čestné prohlášení, že dopis byl doručen na chybnou adresu, neznámého adresáta, či zemřelého.

**Poděkování:** pokud uživatel vybere jednu z výše uvedených možností a potvrdí ji, Je mu poděkováno. 
Poslední možností je přihlášení se jako nový poplatník, kde se postupuje stejně jako u scénáře Domácnost (FO) - Přihlášení k platbě TV poplatku s tím rozdílem, že již nedochází k autorizaci emailu a k identifikaci osoby (to je součástí výzvy a načítá se ze ZISu). Kroky jsou:

1. **Počátek platby:** uživatel zadává od jakého data se hodlá k platbě přihlásit Je možné zadat datum aktuálního dne nebo jakéhokoliv dne zpět až do doby vzdálené tři roky. 
2. **Periodicita:** Uživatel si vybere interval platby  - měsíční, čtvrtletní, půlroční nebo roční. Doporučen je měsíční.
3. **Platební metoda:** Uživatel volí platební metodu - opakovaná platba kartou  (pouze u měsíční periodicity), jednorázově (karta, google pay, platební bránou) a nebo převodem z bankovního účtu (zde je možné nepovinně zadat číslo účtu). 
4. **Nastavení hesla:** nastavení hesla a souhlas ze zpracováním osobních údajů. Síla hesla není definována ČT ani žádnou jinou autoritou.
5. **Shrnutí:** Uživatel je seznámen se všemi nastaveními které proběhl a je vyzván k finálnímu potvrzení. Popřípadě může volit kroky zpět a nastavení upravit. 
6. **Dokončení:** Uživatel je informován o stavu zápisu do ZIS a zároveň je mu poslán příslušný email. Dle volby platby je mu nabídnuta možnost zaplatit, autorizovat platbu nebo odejít do účtu poplatníka.

 