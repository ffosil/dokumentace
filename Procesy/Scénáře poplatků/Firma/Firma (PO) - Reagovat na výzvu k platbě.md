Jedná se o scénář, kdy se uživatel (PO) na základě výzvy (zaslané dopisem) reaguje. Reagovat může jednak jednou z nabízených odpovědí (resp. Prohlášením), popřípadě založením nového účtu poplatníka.  Kromě úvodní informační stránky a stránek pro registraci nového poplatníka tento scénář obsahuje 5 kroků. 

**Ověření:** Na základě výzvy uživatel nastaví kod oslovení, kontaktní email a kód ověření.  Po ověření je uživatele zaslán autorizační link  pro pokračování.

**Nastavení evidence:** Jedná se o rozcestník, kde uživatel určuje jak chce na výzvu reagovat. Možnosti jsou buď čestné prohlášení že TV nevlastní nebo se přihlásit jako nový poplatník:

* **Nevlastním žádný TV přijímač:** čestné prohlášení, že nevlastní TV přijímač.

**Poděkování:** pokud uživatel vybere jednu z výše uvedených možností a potvrdí ji, Je mu poděkováno.

Poslední možností je přihlášení se jako **nový poplatník**, kde se postupuje stejně jako u scénáře Firma (PO) - Přihlášení k platbě TV poplatku s tím rozdílem, že již nedochází k autorizaci emailu a k identifikaci osoby (to je součástí výzvy a načítá se ze ZISu). Kroky jsou:

* **Počet přijímačů:** Uživatel zadává počet přijímačů na všech provozovnách, Přičemž poplatek hradí za každý přijímač. Počet přijímačů lze později pouze navýšit, pro snížení je potřeba využít autorizovaného požadavku v Účtu poplatníka.
* **Počátek platby:** uživatel zadává od jakého data se hodlá k platbě přihlásit Je možné zadat datum aktuálního dne nebo jakéhokoliv dne zpět až do doby vzdálené tři roky. 
* **Periodicita:** Uživatel si vybere interval platby  -  čtvrtletní, půlroční nebo roční.
* **Platební metoda:** Uživatel volí platební metodu - jednorázově (karta, google pay, platební bránou) a nebo převodem z bankovního účtu (zde je možné nepovinně zadat číslo účtu). 
* **Nastavení hesla:** nastavení hesla a souhlas ze zpracováním osobních údajů. Síla hesla není definována ČT ani žádnou jinou autoritou.
* **Shrnutí:** Uživatel je seznámen se všemi nastaveními které proběhl a je vyzván k finálnímu potvrzení. Popřípadě může volit kroky zpět a nastavení upravit. 
* **Dokončení:** Uživatel je informován o stavu zápisu do ZIS a zároveň je mu poslán příslušný email. Dle volby platby je mu nabídnuta možnost zaplatit platbu nebo odejít do účtu poplatníka.