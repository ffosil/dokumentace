Jedná se o základní scénář, kdy se uživatel (PO) dobrovolně rozhodně pro přihlášení se k televizním poplatkům. Kromě statické stránky s informacemi má 10 kroků.

**Autorizace emailu:** Zaslání autorizačního linku na zadaný email a souhlas se zpracováním údajů

**Nastavení hesla:** síla hesla není definována ČT ani žádnou jinou autoritou. 

**Firemní údaje:** Uživatel zadává IČ a na základě tohoto zadání se načtou příslušné údaje se systému ARES.

**Počet přijímačů:** Uživatel zadává počet přijímačů na všech provozovnách, Přičemž poplatek hradí za každý přijímač.

**Počátek platby:** uživatel zadává od jakého data se hodlá k platbě přihlásit Je možné zadat datum aktuálního dne nebo jakéhokoliv dne zpět až do doby vzdálené tři roky. 

**Periodicita:** Uživatel si vybere interval platby  -  čtvrtletní, půlroční nebo roční. 

**Platební metoda:** Uživatel volí platební metodu - jednorázově (karta, google pay, platební bránou) a nebo převodem z bankovního účtu (zde je možné nepovinně zadat číslo účtu). 

**Shrnutí:** Uživatel je seznámen se všemi nastaveními, které proběhl a je vyzván k finálnímu potvrzení. Popřípadě může volit kroky zpět a nastavení upravit. 

**Dokončení:** Uživatel je informován o stavu zápisu do ZIS a zároveň je mu poslán příslušný email. Dle volby platby je mu nabídnuta možnost zaplatit platbu nebo odejít do účtu poplatníka. 

**Stav platby:** Uživatel je po zaplacení přesměrován na tuto stránku, kde je seznámen s průběhem platby.