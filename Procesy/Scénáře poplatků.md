Hlavní částí WPČT je šest scénářů, které řeší základní požadavek návštěvníků a to přihlášení se popř. převod poplatků. Scénáře se liší jednak zacílením - to znamená zda-li se jedná o fyzickou nebo právnickou (firma) osobu. A dále, tím, zda-li se realizují přes účet poplatníka (v tomto případě není nutná autorizace emailu a zadání hesla) nebo se jedná o nového poplatníka.

Postupy ve scénářích jsou v určité čísti kroků stejné a používají se pro ně výhodně, stejné react komponenty.

## Vstupní scénáře (jeich dokončení vytváří uživatelský účet)
- **Domácnost (FO)**
    - [Přihlášení k platbě TV poplatku](Scénáře poplatků/Domácnost/Domácnost (FO) - Přihlášení k platbě TV poplatku)
    - [Reagovat na výzvu k platbě](Scénáře poplatků/Domácnost/Domácnost (FO) - Reagovat na výzvu k platbě)
    - [Převod ze SIPO na přímou platbu](Scénáře poplatků/Domácnost/Domácnost (FO) - Převod ze SIPO na přímou platbu)
- **Firma (PO)**
    - [Přihlášení k platbě TV poplatku](Scénáře poplatků/Firma/Firma (PO) - Přihlášení k platbě TV poplatku)
    - [Reagovat na výzvu k platbě](Scénáře poplatků/Firma/Firma (PO) - Reagovat na výzvu k platbě)

## Scénáře v uživatelském účtu
- **Domácnost (FO)**
    - [Přihlášení k platbě TV poplatku](Scénáře poplatků/Účet poplatníka/Domácnost/Účet poplatníka - Domácnost (FO) - Přihlášení k platbě TV poplatku)
    - [Převod ze SIPO na přímou platbu](Scénáře poplatků/Účet poplatníka/Domácnost/Účet poplatníka - Domácnost (FO) - Převod ze SIPO na přímou platbu)
    - [Reagovat na výzvu k platbě](Scénáře poplatků/Účet poplatníka/Domácnost/Účet poplatníka - Domácnost (FO) - Reagovat na výzvu k platbě)
- **Firma (PO)**
    - [Přihlášení k platbě TV poplatku](Scénáře poplatků/Účet poplatníka/Firma/Účet poplatníka - Firma (PO) - Přihlášení k platbě TV poplatku)
    - [Reagovat na výzvu k platbě](Scénáře poplatků/Účet poplatníka/Firma/Účet poplatníka - Firma (PO) - Reagovat na výzvu k platbě)
- **společné**
    - [Změna typu platby](Scénáře poplatků/Účet poplatníka/Účet poplatníka - změna typu platby)
    - [Zaplacení TV poplatku](Scénáře poplatků/Účet poplatníka/Účet poplatníka - zaplacení TV poplatku)
    - [Registrovat existující VS](Scénáře poplatků/Účet poplatníka/Účet poplatníka - registrovat existující VS)

