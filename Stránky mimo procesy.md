## Vstupní stránky

Za homepage resp. Vstupní stránky se považují stránky Domácnosti (https://poplatky.ceskatelevize.cz/domacnost) a Firmy (https://poplatky.ceskatelevize.cz/firma). Jedná se o statické rozcestníky. 

## Otázky a odpovědi (FAQ)
Tyto stránky se doplňují z redakčního systému frameworku Django a mohou fungovat jednak jako samostatná stránky (https://poplatky.ceskatelevize.cz/otazky-a-odpovedi/vypis) nebo jako modulární součást definované stránky - např. zde - https://poplatky.stagect.cz/domacnost.

## Přišla mi / Potřebuji vyřešit (FO i PO)
- Upomínky a žaloby: [upomínka](https://poplatky.ceskatelevize.cz/upominka-zaloba-dino#upominka), [žaloba](https://poplatky.ceskatelevize.cz/upominka-zaloba-dino#zaloba)
- [Osvobození od platby TV poplatku](https://poplatky.ceskatelevize.cz/osvobozeni-od-platby)
- [Odhlášení platby TV poplatku pro domácnosti](https://poplatky.ceskatelevize.cz/odhlaseni-platby)
- [Dekódovací karty](https://poplatky.ceskatelevize.cz/dekodovaci-karty)
- [Změnu počtu TV přijímačů](https://poplatky.ceskatelevize.cz/zmena-poctu-tv-prijimacu)
- [Odhlášení platby TV poplatku pro firmy](https://poplatky.ceskatelevize.cz/odhlaseni-platby-firma)

### [Kontakty](https://poplatky.ceskatelevize.cz/kontakty)
Včetně kontaktního formuláře

## Dočasné stránky
Z důvodu občasné akce **Milostivé léto** je ve struktuře i tato stránka, která se spouští dle potřeby

## Ostatní stránky
- [404](https://poplatky.ceskatelevize.cz/404)
- [Landing page pro výzvu](https://poplatky.ceskatelevize.cz/prohlaseni) (historická stránka řešená redirectem)