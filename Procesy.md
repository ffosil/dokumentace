Procesy, které probíhají v rámci aplikace lze rozdělit do následujících kategorií:

- [Správa uživatelů](Procesy/Správa uživatelů)
- [Účet poplatníka](Procesy/Účet poplatníka)
- [Scénáře poplatků](Procesy/Scénáře poplatků)
- [PInfo](Procesy/PInfo)
- [Kontaktní formulář](Procesy/Kontaktní formulář)
- [Odesílané emaily](Procesy/Odesílané emaily)