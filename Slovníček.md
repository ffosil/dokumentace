**Aktivní formulář (AF):** předefinovaný způsob zápisu do ZISu. Dle AF se určuje, co se má v ZISu stát.

**ARES:** veřejný přístup do databáze IČ firem. V rámci WPČT používán pro ověření existence IČ.

**Autorizační platba:** první platba kartou, při opakovaných platbách. Platební bráně potvrdí vůli poplatníka využívat opakovanou platbu.

**FPUZ:** systém který používají OPČT pro zdávání požadavků na ZIS.

**Oddělení poplatků ČT (OPČT):** oddělení v rámci ČT, která má na starosti výběr a správu výběru koncesionářských poplatků

**Opakované platby:** automatické opakované platby, které může FO osoba využívat pro platbu poplatků. Opakované platby jsou automaticky stahované z platební brány. 

**PInfo:** historický systém generování informací o jednorázových platbách. Zajišťuje ZIS. V rámci WPČT se pouze zapíná a vypíná v rámci účtu poplatníka. 

**Poplatník:** subjekt, který platí koncesionářské poplatky. Může se jednat o fyzickou nebo právnickou osobu. 

**Právní řešení:** stavy, kdy má poplatník dluh, který je již postoupen advokátní nebo exekuční kanceláři. OPČT se nimi nemůže nakládat ani stav upravovat. Rovněž uživatel nemůže v účtu poplatníka nic nastavovat. 

**RUIAN:** státní systém udržující všechny adresy v ČR. Slouží k ověřování a zadávání adres v rámci WPČT

**Televizní poplatek:** též koncesionářský poplatek, částka daná státem, která je vybírána od poplatnka každý měsíc. 

**Variabilní symbol, entita poplatníka, poplatné místo:** reálná entita, za kterou je vybírán koncesionářský poplatek. U fyzických osob se jedná o domácnost u právnických osob je to za každý vlastněný televizní přístroj. 

**Účet poplatníka:** prostor na WPČT kde může uživatel po přihlášení nahlížet a spravovat svá poplatná místa či kontaktní údaje. 

**USYS:** společnost spravující ZIS

**Výzva (někdy též akvizice):** jedná se o výzvu (nejčastěji ve formě dopisu), který se rozesílá subjektům, kteří nejsou plátci konc. poplatků, ale je u nich předpoklad, že vlastní televizní přijímač. WPČT umožňuje výzvu spracovat. 

**Webová aplikace poplatků ČT (WPČT):** webová aplikace poplatků ČT. Prosto na webu ČT, kde je možné vytvářet či spravovat uživatelské účty poplatníků vč. správy entit poplatníka, či možnosti platby. 

**ZIS:** databázový a workflow systém udržující veškeré informace o poplatnících ČT (mimo jiné). Pomocí API je možné do ZISu upravovat (nebo získávat) potřebné údaje o poplatnících. ZIS spravuje společnost USYS.