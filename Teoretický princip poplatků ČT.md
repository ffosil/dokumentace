# Platba televizních poplatků
Dle zákona 348 O rozhlasových a televizních poplatcích a o změně některých zákonů je definována povinnost určité skupiny obyvatel a firem hradit koncesionářské poplatky (dále jen KP nebo poplatky).  V současné době (6/2022) je výše koncesionářského poplatku 135 Kč. Princi platby KP je rozdílný pro domácnosti (fyzické osoby) a pro firmy (právnické osoby). Domácnosti hradí KP – 135 Kč za celou domácnost bez ohledu kolik televizních přijímačů vlastní a používají. Rovněž nezáleží na místě, kde ho používají (domácnost, rekreační ubytování apod). Poplatek v pronájmu hradí nájemce (pronájemce). Fyzická osob může požádat o zrušení platby poplatků buď ze zdravotních nebo sociálních důvodů. Rovněž může podat čestné prohlášení, že televizní přijímač nevlastní.

Naproti tomu právnické osoby hradí poplatek za každý vlastněný televizní přijímač, ať už je například na recepci nebo v hotelových pokojích.

## Interval a počátek platby poplatků
Poplatky je možné hradit buď měsíčně, čtvrtletně, půlročně nebo ročně, vždy na začátku daného období. Poplatek je v každém případě vždy 135 Kč za měsíc. Tedy 135 Kč za měsíc, 405 Kč za čtvrtletí, 810 Kč za pololetí a 1620 Kč za rok. Pokud není poplatek uhrazen prvním měsíci daného období pohlíží se na něj jako na neuhrazený. Po určité době se tento stav překlopí do tzv. právního řešení. V tomto stavu již není možné poplatek regulárně uhradit a je nutné ho řešit s příslušným právním subjektem (advokátní a exekutorské kanceláře), kterým ale není Česká televize. Ta do procesu již nemůže zasahovat. 

Každý poplatník si při registraci definuje datum počátku vlastnictví televizního přijímače. Toto datum může být definováno až tři roka zpět od aktuálního dne. Poplatek se následně hradí od následujícího měsíce. Pokud poplatník volí delší, něž měsíční interval platby, jako první uhradí doplatek ze zbývajícího období. Například pokud se přihlásí v dubnu k poplatkům s interval platby půl roku , uhradí nejdříve poměrovou částku za květen a červen. Následně od července platí již půlročně.

Poplatník má právo interval (i způsob platby) měnit. Vzhledem ke složitosti výpočtu dopočítaných částek, tak může činit pouze v posledním měsíci aktuálně placeného období. Tedy pokud má interval měsíční  - může změnu provést kdykoliv. Pokud má interval čtvrtletní může změnu provést v březnu, červnu, září a prosinci. U pololetního intervalu v červnu a prosinci a u ročního intervalu pouze v prosinci. 

Právnické osoby mohou platit v intervalu čtvrtletním, pololetním nebo ročním.

## Způsoby platby televizních poplatků

Domácnosti mohou pro platbu využít buď SIPO (pouze historicky nebo na poště, není možné se k platbě SIPO přihlásit přes webovou aplikaci ČT), klasický převod z bankovního účtu nebo jednorázově pomocí platební karty, Google pay nebo pomocí platební brány (aktuálně zajišťuje Comagte). Všechny tyto platby si plátce kontroluje a zajišťuje sám. 

Domácnosti navíc mohou využít tzv. opakované platby. Ty lze použít pouze při měsíčním intervalu platby.  V tomto přístupu jsou poplatníkovi poplatky automaticky strhávány z platební karty. Servis aktuálně zajišťuje společnost Comgate (6/2022). U tohoto způsobu platby poplatník nejdříve uhradí autorizační platbu. Tato platba je zároveň i první platbou. Ti znamená, že další platba je až následující měsíc. Příklad: Poplatník se přihlásí v dubnu k opakované platbě. Ihned provede autorizační platbu, která je zároveň i první platbou za měsíc květen. Další platba se tedy strhne až v červnu. 

Žádost o opakovanou platbu je vyvolána na straně WPČT a to vždy 10. dne v měsíci, voláním do ZIS, který vrátí seznam neuhrazených opakovaných plateb pro aktuální měsíc. WPČT následně zasílá žádosti na platební bránu. Vzhledem k tomu, že ne všechny platby jsou úspěšné (v drtivé většině z důvodu na straně plátce poplatků) celý proces se opakuje ještě 20. dne v měsíci. Plátcům, kteří mají nastaveny opakované platby, ale nepodařilo se v daném měsíci ani jednou stáhnout opakovanou platbu, je zaslán email s informací, že jejich typ platby byl převeden na jednorázovou platební metodu plus se tento převod uskuteční. 

Systém poplatků umožňuje u opakovaných plateb kontrolu časové platnosti karty. Měsíc před vypršením platnosti je uživatel emailem vyzván k zadání nové karty a k provedení nové autorizační platby.

Právnické osoby mohou využívat k platbě pouze klasický převod z účtu nebo jednorázově (platební karty, Google pay, platební brána).

Při volbě jednorázové platby si může poplatník zaškrtnout možnost zasílání informací o aktuální platbě. Tato funkce je historická a zajišťuje ji USYS, kde se nazývá pInfo. 

Jednorázové platby je možné rovněž provést přes rozhraní webové  aplikace poplatků ČT.

## Vztahy mezi jednotlivými entitami
Celý projekt je propojením několika subjekty. V rámci struktury ČT má poplatky z pohledu účetního n starosti oddělení poplatku (dále OPČT).  Nicméně veškerou správu a dohled nad   daty má společnost USYS. Tato společnost obsluhuje systém ZIS, kde jsou uloženy veškeré informace o poplatnících, včetně plateb a administrativních informací. Rovněž zajišťuje rozesílání emailu na včetně emailů připravený na straně webové aplikace poplatků ČT. Také provádí zúčtování přišlých plateb a přípravu databáze pro rozesílání výzev. OPČT disponuje desktopovou aplikací napojenou na ZIS, pomocí které spravují kompletní požadavky poplatníků. Případné požadavky na změny v desktopové aplikaci nebo obecně ke změně přístupu mezi OPČT a USYS se zapisují OPČT do systému FPUZ.

V omezeném rozsahu si poplatník může spravovat požadavky týkajících se poplatků na webové aplikaci poplatků ČT (dále WPČT).  Jedná se především o novou registraci, reakci na výzvu, propojení webového účtu se stávajícím poplatníkem, nastavení některých údajů změnu typu plateb apod. (detailně popsáno níže v sekci scénářů). WPČT přistupuje od ZIS pomocí API. Zápisy do ZISU (ať před desktopovou aplikaci OPČT nebo přes WTP) jsou identifikovány pomocí tzv aktivní formulářů (zkráceně AF). Ty ZISU definují, o jaký typ zápisu se jedná. Platby poplatků (jednorázové, opakované i autorizační) se realizují na straně WPČT. USYS tyto platby zaúčtuje (po připsání na účet) a zapíše do ZIS. Poplatky se aktuálně realizují přes API společnosti Comgate (06/2022).

![Vztahy entit](media/vztah-entit.png)

## Účet poplatníka

Přihlášení slouží k přístupu k údajům poplatníka do tzv. účtu poplatníka. Poplatník může pod svým webovým účtem více entit poplatků, a to včetně kombinací pro fyzické a pro právnické osoby. ZIS, v kterém jsou veškeré údaje spravovány má velmi univerzální systém správy účtů a vs. Ten je v rámci aplikace poplatků ČT omezen. 

![Systém správy účtů v ZIS](media/webove-ucty-v-ZIS.png)

Oproti tomu je v rámci WPČT uplatňován princi, že jeden variabilní symbol (dále jen VS), tedy jeden entita poplatků, může být vázán jen na jeden webový účet. Tedy, že každé VS může být vázáno jen na jeden účet, ale účet může mít více VS. 

Další nastavení, které ZIS umožňuje ale není v WPČT použito je princip partnerský id. Ty se v praxi mohou použít pro rozdělení jednoho místa poplatníka na více VS – standardně například rozdělení provozovny u právnické osoby. V současné době se tento přístup v rámci poplatků nepoužívá. 

Webový účet sám so sobě může být teoreticky i prázdný. Lze ho vytvořit jak pro nové VS tak k němu připojit existující VS. V současné době nelze VS ani rušit ani převáděn na jiný webový účet. Rovněž je potřeba mít na paměti, že v budoucnosti bude v ČT existovat univerzální webový účet, který by měl pokrývat všechny služby nabízené ČT (iVysílání, poplatky, EDU apod.). 

Z pohledu účtování je důležité v jakém stavu je účet. Stavu účtu může být:

- **oslovený z dopisu** – tj. USYS i ČT, předpokládá, že subjekt má TV a pomocí dopisu ho osloví a snaží se ho přesvědčit k nějaké reakci (tj. Registraci, nebo čestné prohlášení o nevlastnění TV)
- **oslovený z WPČT** - tj. Uživatel, který si poprvé vytvořil nějakým způsobem účet na webu poplatků ČT, ale ještě neprovedl první nebo autorizační platbu (resp. Dokud není platba zaúčtována v ZIS). Reálně se uživatel stává poplatníkem až v momentě zaúčtování první platby. 
- **aktivní poplatník** - poplatník, který platí a má vše v pořádku zaplaceno, popřípadě, nemá zaplaceno, ale ještě nebyl postoupen do právního řešení. 
- **účet v právním řešení** - dlužnou částku má nyní ve správě účastník v právním řešení (exekutorská nebo advokátní kancelář) a teprve po vyřešení právního řešení se účet vrací do běžného stavu, nebo je zrušen. 
- **zrušený účet** - v tomto případě nemůže poplatník s účtem nakládat, ale může do něj do dobu 3 měsíce nahlížet.