# Dokumentace k webové aplikace Poplatky ČT

_Filip Novák a Matěj Grulich_

Následující dokumentace si klade zas cíl seznám jak s obecným fungování koncesionářský poplatků ČT tak i technický popis fungování celé infrastruktury webové aplikace. Vzhledem k rozsahu není možné zcela pokrýt všechny aspekty zmíněné aplikace, nicméně dokládá všechny části, metody a komponenty a jejich vzájemnou provázanost.

V textu se vyskytuje řada ustálených výrazů, pro snadnější orientaci v těchto výrazech je na konci dokumentu připraven slovníček těchto pojmů.

## Obsah

1. [Teoretický princip poplatků ČT](Teoretický princip poplatků ČT)
2. [Sitemap](Sitemap)
3. [Procesy](Procesy)
4. [Stránky mimo procesy](Stránky mimo procesy)
5. [Struktura aplikace a technické řešení](Struktura aplikace a technické řešení)
    - [Frontend](Struktura aplikace a technické řešení/Frontend)
    - [Backend](Struktura aplikace a technické řešení/Backend)
    - [Externí služby](Struktura aplikace a technické řešení/Externí služby)
6. [Slovníček](Slovníček)
